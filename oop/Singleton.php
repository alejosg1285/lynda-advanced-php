<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 1:27 PM
     */

    class Singleton
    {
        protected static $instance;

        protected function __construct() {
        }

        public static function getInstance() {
            if (null === static::$instance) {
                static::$instance = new Static();
            }

            return static::$instance;
        }

        protected function __clone() {
            // prevent cloning of the instance
        }

        protected function __wakeup() {
            // TODO: Implement __wakeup() method.
        }
    }

    class SingletonChild extends Singleton {
        protected static $instance;
    }

    $obj = Singleton::getInstance();
    var_dump($obj === Singleton::getInstance());

    $anotherObj = SingletonChild::getInstance();
    var_dump($anotherObj === Singleton::getInstance());

    var_dump($anotherObj === SingletonChild::getInstance());