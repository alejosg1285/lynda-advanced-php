<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 1:23 PM
     */

    class Structs
    {
        public function __construct($input) {
            echo  "{$input}\n";
        }

        public function __destruct() {
            echo 'destruct\n';
        }
    }

    $obj = new Structs('construct');