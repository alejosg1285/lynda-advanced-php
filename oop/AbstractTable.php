<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 1:54 PM
     */

    abstract class Database
    {
        abstract public function connection();

        public function disconnect() {
            echo 'disconnect from database\n';
        }
    }

    class Table extends Database {

        public function connection() {
            echo 'connect to database';
        }
    }

    $mysql = new Table();