<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 4:48 PM
     */
    class InvalidCCNumberException extends InvalidArgumentException {
        public function __construct($message = "No CC number", $code = 0, Throwable $previous = null) {
            return parent::__construct($message, $code, $previous);
        }
    }

    try {
        processCC();
    } catch (InvalidCCNumberException $e) {
        echo $e->getMessage();
        echo '<br>';
        echo get_class($e);
        echo '<br>';
        /*echo $e->getPrevious()->getMessage();
        echo '<br>';
        echo get_class($e->getPrevious());*/
    } finally {
        echo 'finally<br>';
    }

    function processCC($numb = null, $zipCode = null) {
        try {
            validate($numb, $zipCode);
        } catch(Exception $e) {
            //throw new BadFunctionCallException('Invalid inputs', null, $e);
            throw $e;
        }
        echo 'processed';
    }

    function validate($numb, $zipCode) {
        if (is_null($numb))
            //throw new InvalidArgumentException('No CC number');
            throw new InvalidCCNumberException();
    }