<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 2:59 PM
     */
    $passwd = password_hash('testing', PASSWORD_DEFAULT);
    echo "{$passwd}<br>";
    $match = password_verify('foo', $passwd);
    echo "{$match}<br>";
    $match = password_verify('testing', $passwd);
    echo "{$match}<br>";

    if (password_needs_rehash($passwd, PASSWORD_DEFAULT, ['cost' => 12])) {
        $newHash = password_hash('testing', PASSWORD_DEFAULT, ['cost' => 12]);
        echo "{$newHash}<br>";
    }