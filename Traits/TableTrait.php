<?php

    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 1:07 PM
     */
    trait Log
    {
        protected function log($msg) {
            echo "{$msg}\n";
        }
    }

    class Table
    {
        use Log;

        public function save() {
            $this->log('Save start');
        }
    }

    (new Table())->save();