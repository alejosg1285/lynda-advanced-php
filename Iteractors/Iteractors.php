<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 2:13 PM
     */

    class BasicIterator extends IteratorIterator
    {
        public function __construct($filePath) {
            parent::__construct(new SplFileObject($filePath, 'r'));

            $file = $this->getInnerIterator();
            $file->setFlags(SplFileObject::READ_CSV);
            $file->setCsvControl(',', '"', '\\');
        }
    }

    class FilterRow extends FilterIterator {

        /**
         * Check whether the current element of the iterator is acceptable
         * @link  http://php.net/manual/en/filteriterator.accept.php
         * @return bool true if the current element is acceptable, otherwise false.
         * @since 5.1.0
         */
        public function accept() {
            $current = $this->getInnerIterator()->current();
            if (count($current) == 1) {
                return false;
            }

            return true;
        }
    }

    $filePath = './example.csv';
    $iterator = new BasicIterator($filePath);
    $iterator = new FilterRow($iterator);

    foreach ($iterator as $i => $row) {
        var_dump($row);
    }