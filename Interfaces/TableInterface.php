<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 12:41 PM
     */

    interface TableInterface
    {
        public function save(array $data);
    }

    interface LogInterface
    {
        public function log($message);
    }

    class Table implements TableInterface, LogInterface {

        public function save(array $data) {
            return 'foo';
        }

        public function log($message) {
            return sprintf(" %s \n", $message);
        }
    }

    echo (new Table())->save([]);
    echo (new Table())->log('Learning to use interfaces');