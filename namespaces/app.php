<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 11:17 AM
     */

    namespace App;

    include 'Project.php';

    use Project\Table as ProjectTable;

    class Table
    {
        public static function get() {
            echo "App.Table.get\n";
        }
    }

    ProjectTable::get();
    Table::get();