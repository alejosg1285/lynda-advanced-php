## Using namespace PHP!

[PHP namespace](https://www.php.net/manual/en/language.namespaces.php)
 

**Table.php**

    <?php
	    namespace Project;
	    
	    class Table {
		    public static function get() {
			    echo 'Project.Table.get \n';
		    }
	    }

**App.php**

    <?php
	    namespace App;
	    
	    include 'Table.php';
	    
	    use Project\Table as ProjectTable;

	    class Table {
		    public static function get() {
			    echo 'App.Table.get \n';
		    }
	    }
	    
	    Table::get();
	    ProjectTable::get();

**Execution**

    $ php App.php
    App.Table.get
    Project.Table.get

