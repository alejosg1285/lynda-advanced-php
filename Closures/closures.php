<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 4:28 PM
     */
    $arr = [1,2,3,4,5,6,7,8,9];
    $out = array_filter($arr, function ($item) {
        return ($item % 2 === 0);
    });
    print_r($out);
    print_r($arr);

    $filterFunc = function ($item) {
        return ($item % 2 === 1);
    };
    $out = array_filter($arr, $filterFunc);
    print_r($out);