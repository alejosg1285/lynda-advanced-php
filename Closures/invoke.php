<?php

    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 7/07/2019
     * Time: 4:34 PM
     */
    class isPositiveInt
    {
        public function __invoke($value) {
            return ((int)$value == $value && $value > 0);
        }
    }

    $invoke = new isPositiveInt();
    var_dump($invoke(5));
    var_dump($invoke('5'));
    var_dump($invoke(5.0));
    var_dump($invoke(-5));
    var_dump($invoke(5.1));